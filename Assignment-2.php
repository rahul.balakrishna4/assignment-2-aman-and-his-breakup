<?php
class Assignment2 
{
	function check_if_word_is_present($input_string_param = '', $sub_string_param = '')
	{
		$input_string 			= htmlentities(strip_tags(trim($input_string_param)));
		$sub_string 			= htmlentities(strip_tags(trim($sub_string_param)));
		$is_substring_present	= false;

		if(strlen($input_string) <= 1 || 
			(strlen($input_string) == strlen($sub_string)) || 
			strlen($input_string) >= 100)
		{
			return "Let us breakup!";
		} 

		for($index_counter = 0; $index_counter < strlen($sub_string); $index_counter++)
		{
			if(substr_count($input_string, $sub_string[$index_counter]))
			{
				$is_substring_present 	= true;
			}
			else 
			{
				$is_substring_present	= false;
				break;
			}
		}

		if($is_substring_present === true)
		{
			return "I love you, too!";
		}
		else
		{
			return "Let us breakup!";
		}
	}
}

$assignment_two	= new Assignment2();
echo $assignment_two->check_if_word_is_present('lvo3333333asdafajfgnkdefn33333', 'love');
